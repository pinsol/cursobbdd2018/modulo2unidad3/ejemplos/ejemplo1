<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "depart".
 *
 * @property int $dept_no
 * @property string $dnombre
 * @property string $loc
 */
class Depart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'depart';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dept_no'], 'integer'],
            [['dnombre', 'loc'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dept_no' => 'Dept No',
            'dnombre' => 'Dnombre',
            'loc' => 'Loc',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emple".
 *
 * @property int $emp_no
 * @property string $apellido
 * @property string $oficio
 * @property int $dir
 * @property string $fecha_alt
 * @property int $salario
 * @property int $comision
 * @property int $dept_no
 */
class Emple extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emp_no', 'apellido'], 'required'],
            [['emp_no', 'dir', 'salario', 'comision', 'dept_no'], 'integer'],
            [['fecha_alt'], 'safe'],
            [['apellido'], 'string', 'max' => 50],
            [['oficio'], 'string', 'max' => 30],
            [['emp_no'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'emp_no' => 'Código de Empleado',
            'apellido' => 'Apellido',
            'oficio' => 'Oficio',
            'dir' => 'Dirección',
            'fecha_alt' => 'Fecha de Alta',
            'salario' => 'Salario',
            'comision' => 'Comisión',
            'dept_no' => 'Código´ del Departamento',
        ];
    }
}
